resource "aws_instance" "docker-machine1" {
  ami           = "ami-0c7217cdde317cfec"
  instance_type = "t2.micro"
  key_name = "terraform"
  security_groups = ["allow_ssh", "allow_http", "allow_egress", "allow_8080", "allow_mysql"]

  user_data = file("script.sh")

  tags = {
    Name = "docker-machine1"
  }
}

